Mistrovstvý v ledním hokeji databázový systém do předmětu IIS
=============================================================

phillwide.cz

Autoři: Filip Široký, Juraj Škandera

Na phillwide.cz je původní odevzdaná verze

Od odevzdání opraveno:
 - neplatný link v dokumentaci
 - opraven název úpravy uživatelských účtů v app/presenters/templates/@layout.latte
 - opraven button v detail týmu (chyběla class btn-primary) - editovat hráče v app/presenters/templates/Team/more.latte
 - upraveny skupina a pozice selecty v detailu týmu - nebyl nastaven css styl v app/presenters/Team/*

Implementační prostředky:
 - Nette framework 2.4
 - bootstrap 3.3.7
 - daterangepicker 2
 - jquery 3.2.1
 - momentjs 2.18.1
 
 Licence v souboru LICENSE