<?php

namespace App\Forms;

use Nette;
use Nette\Application\UI\Form;


class CreateMatchForm extends Nette\Object
{
	private $database;

	public function __construct(Nette\Database\Context $databaza)
	{
		$this->database = $databaza;
	}

	public function create()
	{
		$form = new Form;

		$renderer = $form->getRenderer();
	 	$renderer->wrappers['error']['container'] = 'div class="alert alert-danger alert-dismissible"';	
		$renderer->wrappers['controls']['container'] = null;
		$renderer->wrappers['pair']['container'] = 'div class="form-group"';
		$renderer->wrappers['pair']['.error'] = 'has-error';
		$renderer->wrappers['label']['container'] = 'div class="col-sm-2 control-label"';
		$renderer->wrappers['control']['description'] = 'span class=help-block';
		$renderer->wrappers['control']['errorcontainer'] = 'span class=help-block';
		$renderer->wrappers['control']['.text'] = 'form-control';
		$renderer->wrappers['control']['.password'] = 'form-control';
		$renderer->wrappers['control']['.file'] = 'form-control';
		$renderer->wrappers['control']['.email'] = 'form-control';
		$renderer->wrappers['control']['.number'] = 'form-control';
		$renderer->wrappers['control']['.button'] = 'btn btn-primary';
		$renderer->wrappers['control']['.submit'] = 'btn btn-primary';

		$domaci_val = array();
		$domaci = $this->database->table('tym');
		foreach ($domaci as $key => $value)
		{
			$domaci_val[$key] = $value->ID_tym;
		}

		$form->addSelect('domaci','*Domáci', $domaci_val)->setRequired()
			->setAttribute("class", "form-control");;

		$form->addSelect('hostia','*Hosté', $domaci_val)->setRequired()->addRule(Form::NOT_EQUAL,'Nemohou být dva stejné týmy', $form['domaci'])
			->setAttribute("class", "form-control");;

		$form->addText('misto_konani', '*Místo Konání')->setRequired();


		$oznaceni_val = array();
		$oznaceni = $this->database->table('faza');
		foreach ($oznaceni as $key => $value) 
		{
			$oznaceni_val[$key] = $value->oznacenie;
		}

		$form->addSelect('oznacenie', '*Označení', $oznaceni_val)->setRequired()
			->setAttribute("class", "form-control");
		
		$form->addText('datum_konani', '*Datum Konání')
			->setAttribute("value", date("d.m.Y"))
			->setOption('description', 'dd.mm.rrrr')
    			->addRule($form::PATTERN, "Datum musí být ve formátu dd.mm.rrrr", "(0[1-9]|[12][0-9]|3[01])\.(0[1-9]|1[012])\.(19|20)\d\d")->setRequired();

		$form->addSubmit('create', 'Vytvořit');

		$form->onSuccess[] = array($this, 'succ');
		return $form;

	}

	public function succ(Form $form, $hodnoty)
	{
		$hodnoty_zapas = array();
		$hodnoty_zapas['domaci'] = $hodnoty['domaci'];
		$hodnoty_zapas['hostia'] = $hodnoty['hostia'];
		$hodnoty_zapas['misto_konani'] = $hodnoty['misto_konani'];
		$hodnoty_zapas['datum_konani'] = $hodnoty['datum_konani'];
		$hodnoty_zapas['oznacenie'] = $hodnoty['oznacenie'];

		$id = $this->database->table('zapas')->insert($hodnoty_zapas);
	
		$form->getPresenter()->flashMessage('Úspěšne přidané!', 'alert alert-success alert-dismissible');

		$form->getPresenter()->redirect('Match:Show');
	}

}