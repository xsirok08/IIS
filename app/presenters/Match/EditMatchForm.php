<?php
namespace App\Forms;
use Nette;
use Nette\Application\UI\Form;
use Test\Bs3FormRenderer;

class EditMatchForm extends Nette\Object
{
	private $database;
	public $id;

	public function __construct(Nette\Database\Context $databaza)
	{
		$this->database = $databaza;
	}
	
	public function create()
	{
		$form = new Form;

		$renderer = $form->getRenderer();

 		$renderer->wrappers['error']['container'] = 'div class="alert alert-danger alert-dismissible"';
		$renderer->wrappers['controls']['container'] = null;
		$renderer->wrappers['pair']['container'] = 'div class="form-group"';
		$renderer->wrappers['pair']['.error'] = 'has-error';
		$renderer->wrappers['label']['container'] = 'div class="col-sm-2 control-label"';
		$renderer->wrappers['control']['description'] = 'span class=help-block';
		$renderer->wrappers['control']['errorcontainer'] = 'span class=help-block';
		$renderer->wrappers['control']['.text'] = 'form-control';
		$renderer->wrappers['control']['.password'] = 'form-control';
		$renderer->wrappers['control']['.file'] = 'form-control';
		$renderer->wrappers['control']['.email'] = 'form-control';
		$renderer->wrappers['control']['.number'] = 'form-control';
		$renderer->wrappers['control']['.button'] = 'btn btn-primary';
		$renderer->wrappers['control']['.submit'] = 'btn btn-primary';

		$domaci_val = array();
		$rozhodci_val = array();
		$domaci = $this->database->table('tym');
		foreach ($domaci as $key => $value) {
			$domaci_val[$key] = $value->ID_tym;
		}
		$rozhodci = $this->database->table('rozhodci');
		foreach ($rozhodci as $key => $value) {
			$rozhodci_val[$key] = $value->jmeno_R;
		}

		$form->addSelect('domaci','*Domáci', $domaci_val)->setRequired()
			->setAttribute("class", "form-control");

		$form->addSelect('hostia','*Hosté', $domaci_val)->setRequired()->addRule(Form::NOT_EQUAL,'Nemohou být dva stejné týmy', $form['domaci'])
			->setAttribute("class", "form-control");

		$form->addText('misto_konani', '*Místo Konání')->setRequired();

		$oznaceni_val = array();
		$oznaceni = $this->database->table('faza');
		foreach ($oznaceni as $key => $value)  {
			$oznaceni_val[$key] = $value->oznacenie;
		}
		$form->addSelect('oznacenie', '*Označení', $oznaceni_val)->setRequired() 
			->setAttribute("class", "form-control");
		$rozhodci_val = array();
		$rozhodci = $this->database->table('rozhodci');
		foreach ($rozhodci as $key => $value) {
			$rozhodci_val[$value->jmeno_R] = $value->jmeno_R;
		}
		
		$form->addText('datum_konani', '*Datum Konání')->setAttribute("value", date("d.m.Y"))

			->setOption('description', 'dd.mm.rrrr')

			->addRule($form::PATTERN, "Datum musí být ve formátu dd.mm.rrrr", "(0[1-9]|[12][0-9]|3[01])\.(0[1-9]|1[012])\.(19|20)\d\d")->setRequired();
		$form->addText('vysledok', 'Výsledek')->setAttribute("placeholder", "X:Y");
		$overtime['-'] = '-';
		$overtime['pr'] = 'pr';
		$overtime['sn'] = 'sn';
		$form->addSelect('jmenoH', 'Hlavní rozhodčí', $rozhodci_val)->setRequired(false)
			->setAttribute("class", "form-control");

		$form->addSelect('jmenoV1', 'Vedlejší 1', $rozhodci_val)->setRequired(false)->addRule(Form::NOT_EQUAL,'Dva stejní rozodčí', $form['jmenoH'])
			->setAttribute("class", "form-control");
		$form->addSelect('jmenoV2', 'Vedlejší 2', $rozhodci_val)->setRequired(false)->addRule(Form::NOT_EQUAL,'Dva stejní rozodčí', $form['jmenoH'])
			->addRule(Form::NOT_EQUAL,'Dva stejní rozodčí', $form['jmenoV1'])
			->setAttribute("class", "form-control");
		$form->addSelect('jmenoV3', 'Vedlejší 3', $rozhodci_val)->setRequired(false)->addRule(Form::NOT_EQUAL,'Dva stejní rozodčí', $form['jmenoH'])
			->addRule(Form::NOT_EQUAL,'Dva stejní rozodčí', $form['jmenoV1'])
			->addRule(Form::NOT_EQUAL,'Dva stejní rozodčí', $form['jmenoV2'])
			->setAttribute("class", "form-control");
		$form->addSelect('predlzenie', 'Prodloužení', $overtime)
			->setAttribute("class", "form-control");


		$form->addSubmit('edit', 'Změnit');

		$form->onSuccess[] = array($this, 'success');
		return $form;
	}

	public function success(Form $form, $values)
	{
		$match_f = array();
		$match_f['domaci'] = $values['domaci'];
		$match_f['hostia'] = $values['hostia'];
		$match_f['vysledok'] = $values['vysledok'];
		$match_f['predlzenie'] = $values['predlzenie'];
		$match_f['datum_konani'] = $values['datum_konani'];
		$match_f['misto_konani'] = $values['misto_konani'];
		$record = $this->database->table('zapas')->get($this->id);
		$record->update($match_f);

		$new_record = $this->database->query('DELETE FROM je_pod_dozorom WHERE ID_zapasu='.$this->id);

		$jpd = array();
		$jpd['ID_zapasu'] = $this->id;
		$jpd['pozicia'] = 'H';
		$jpd['jmeno_R'] = $values['jmenoH'];
		$this->database->table('je_pod_dozorom')->insert($jpd);

		$jpd['pozicia'] = 'V';
		$jpd['jmeno_R'] = $values['jmenoV1'];
		$this->database->table('je_pod_dozorom')->insert($jpd);

		$jpd['pozicia'] = 'V';
		$jpd['jmeno_R'] = $values['jmenoV2'];
		$this->database->table('je_pod_dozorom')->insert($jpd);

		$jpd['pozicia'] = 'V';
		$jpd['jmeno_R'] = $values['jmenoV3'];
		$this->database->table('je_pod_dozorom')->insert($jpd);

		$form->getPresenter()->flashMessage('Editováno!', 'alert alert-success alert-dismissible');
		$form->getPresenter()->redirect('Match:more', $this->id);
	}
}