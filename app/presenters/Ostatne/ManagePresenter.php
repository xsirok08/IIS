<?php

namespace App\Presenters;
use Nette;
use Nette\Application\UI\Form;

class ManagePresenter extends BasePresenter {

	private $database;
	
	public function __construct(Nette\Database\Context $databaza)
	{
		$this->database = $databaza;
	}

	public function actionDefault()
	{
		if(!$this->getUser()->isLoggedIn())
		{
			$this->redirect('Sign:in');
		}
		$this->template->users = $this->database->table('uzivatelia');
	}

	public function actionRemoveMe()
	{
		if(!$this->getUser()->isLoggedIn())
		{
			$this->redirect('Sign:in');
		}

		$this->database->table('uzivatelia')->get($this->getUser()->getIdentity()->id)->delete();
		$this->presenter->flashMessage('Úspěch!', 'alert alert-success alert-dismissible');
		$this->presenter->redirect('Sign:in');

	}
	public function actionRemoveUser($id)
	{
		if(!$this->getUser()->isLoggedIn())
		{
			$this->redirect('Sign:in');
		}

		$this->database->table('uzivatelia')->get($id)->delete();
		$this->presenter->flashMessage('Úspěch!', 'alert alert-success alert-dismissible');
		$this->presenter->redirect('Manage:');
	}
	protected function createComponentChangePasswordForm() {

		$form = new Form;
		
		$renderer = $form->getRenderer();
		$renderer->wrappers['error']['container'] = 'div class="alert alert-danger"';
		$renderer->wrappers['controls']['container'] = null;
		$renderer->wrappers['pair']['container'] = 'div class="form-group"';
		$renderer->wrappers['pair']['.error'] = 'has-error';
		$renderer->wrappers['label']['container'] = 'div class="col-sm-2 control-label"';
		$renderer->wrappers['control']['description'] = 'span class=help-block';
		$renderer->wrappers['control']['errorcontainer'] = 'span class=help-block';
		$renderer->wrappers['control']['.text'] = 'form-control';
		$renderer->wrappers['control']['.password'] = 'form-control';
		$renderer->wrappers['control']['.file'] = 'form-control';
		$renderer->wrappers['control']['.email'] = 'form-control';
		$renderer->wrappers['control']['.number'] = 'form-control';
		$renderer->wrappers['control']['.button'] = 'btn btn-primary';
		$renderer->wrappers['control']['.submit'] = 'btn btn-primary';

		$form->addPassword('heslo', '*Nové heslo:', 20)
			->addRule(Form::FILLED, 'Vyplňte Vaše heslo');
		$form->addPassword('password2', '*Nové heslo znovu:', 20)
			->addConditionOn($form['heslo'], Form::VALID)
			->addRule(Form::FILLED, 'Heslo znovu')
			->addRule(Form::EQUAL, 'Hesla se neshodují.', $form['heslo']);
		$form->addPassword('oldpassword', '*Staré heslo:', 20)
			->addConditionOn($form['heslo'], Form::VALID)
			->addRule(Form::FILLED, 'Staré heslo');
		$form->addSubmit('send', 'Změnit heslo');
		$form->onSuccess[] = [$this, 'changepasswordFormSubmitted'];

		return $form;
	}

    	public function changepasswordFormSubmitted(Form $form, $values) {
    		$user = $this->getUser();
    		$name = $user->getIdentity()->id;
    		$new_vals['meno']= $name;
    		$new_vals['heslo'] = $values['heslo'];
    		$new_vals['rola'] = $user->roles; 

    		$this->database->table('uzivatelia')->get($name)->update($new_vals);

    		$form->getPresenter()->flashMessage('Úspěch!', 'alert alert-success alert-dismissible');
		$form->getPresenter()->redirect('Homepage:');

	}
}	