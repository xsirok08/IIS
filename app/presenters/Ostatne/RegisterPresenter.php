<?php

namespace App\Presenters;
use Nette;
use Nette\Application\UI\Form;

class RegisterPresenter extends BasePresenter {

	private $database;

	public function __construct(Nette\Database\Context $databaza)
	{
		$this->database = $databaza;
	}

	protected function createComponentRegisterForm() {

		$form = new Form;

		$renderer = $form->getRenderer();
		$renderer->wrappers['error']['container'] = 'div class="alert alert-danger alert-dismissible"';
		$renderer->wrappers['controls']['container'] = null;
		$renderer->wrappers['pair']['container'] = 'div class="form-group"';
		$renderer->wrappers['pair']['.error'] = 'has-error';
		$renderer->wrappers['label']['container'] = 'div class="col-sm-2 control-label"';
		$renderer->wrappers['control']['description'] = 'span class=help-block';
		$renderer->wrappers['control']['errorcontainer'] = 'span class=help-block';
		$renderer->wrappers['control']['.text'] = 'form-control';
		$renderer->wrappers['control']['.password'] = 'form-control';
		$renderer->wrappers['control']['.file'] = 'form-control';
		$renderer->wrappers['control']['.email'] = 'form-control';
		$renderer->wrappers['control']['.number'] = 'form-control';
		$renderer->wrappers['control']['.button'] = 'btn btn-primary';
		$renderer->wrappers['control']['.submit'] = 'btn btn-primary';

		$form->addText('meno', '*Jméno:');
		$form->addPassword('heslo', '*Heslo:', 20)
			->setOption('description', 'Alespoň 6 znaků')
			->addRule(Form::FILLED, 'Vyplňte Vaše heslo')
			->addRule(Form::MIN_LENGTH, 'Heslo musí mít alespoň %d znaků.', 6);
		$form->addPassword('password2', '*Heslo znovu:', 20)
			->addConditionOn($form['heslo'], Form::VALID)
			->addRule(Form::FILLED, 'Heslo znovu')
			->addRule(Form::EQUAL, 'Hesla se neshodují.', $form['heslo']);
		$form->addSubmit('send', 'Registrovat');

		$form->onSuccess[] = [$this, 'registerFormSubmitted'];

		return $form;
	}

	public function registerFormSubmitted(Form $form) {
		$values = $form->getValues();

		try {
			$new_user_id = $this->register($values);
		} catch (Nette\Database\UniqueConstraintViolationException $e) {
			$form->addError('Chyba registrace. Zkuste zvolit jiné jméno.');
		}

		if ($new_user_id) {
			$this->flashMessage('Registrace byla úspěšná.', 'alert alert-success alert-dismissible');
			$this->redirect('Sign:in');
		}
	}

	public function register($data) {
		unset($data["password2"]);
		$data["rola"] = "guest";
		$data["heslo"] = $data["heslo"];
	
		return $this->database->table('uzivatelia')->insert($data);
	}
}	