<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;

class SignPresenter extends BasePresenter
{
	private $database;
	public function __construct(Nette\Database\Context $databaza)
	{
		$this->database = $databaza;
	}
	protected function createComponentSignInForm()
	{
		$form = new Form;


		$renderer = $form->getRenderer();

	 	$renderer->wrappers['error']['container'] = 'div class="alert alert-danger"';
		$renderer->wrappers['controls']['container'] = null;
        	$renderer->wrappers['pair']['container'] = 'div class="form-group"';
        	$renderer->wrappers['pair']['.error'] = 'has-error';
		$renderer->wrappers['label']['container'] = 'div class="col-sm-2 control-label"';
 		$renderer->wrappers['control']['description'] = 'span class=help-block';
        	$renderer->wrappers['control']['errorcontainer'] = 'span class=help-block';
		$renderer->wrappers['control']['.text'] = 'form-control';
		$renderer->wrappers['control']['.password'] = 'form-control';
		$renderer->wrappers['control']['.file'] = 'form-control';
		$renderer->wrappers['control']['.email'] = 'form-control';
		$renderer->wrappers['control']['.number'] = 'form-control';
		$renderer->wrappers['control']['.button'] = 'btn btn-primary';
		$renderer->wrappers['control']['.submit'] = 'btn btn-primary';

		$form->addText('username', 'Login:')->setRequired('Přihlašovací jméno');

		$form->addPassword('password', 'Heslo:')->setRequired('Heslo');

		$form->addSubmit('send', 'Přihlásit');

		$form->onSuccess[] = [$this, 'signInFormSucceeded'];
		return $form;
	}

	public function signInFormSucceeded(Form $form, Nette\Utils\ArrayHash $values)
	{
		$user = $form->getPresenter()->getUser();
		
		try {
			$user->login($values->username, $values->password);
			$user->setExpiration('30 minutes');
			$this->flashMessage('Přihlášení bylo úspěšné.', 'alert alert-success alert-dismissible');
			$this->redirect('Homepage:');

		} catch (Nette\Security\AuthenticationException $e) {
			$form->addError('Nesprávné přihlašovací jméno nebo heslo.', 'alert alert-danger alert-dismissible');
		}
	}

	public function actionOut()
	{
	    $this->getUser()->logout();
	    $this->flashMessage('Odhlášení bylo úspěšné.', 'alert alert-success alert-dismissible');
	    $this->redirect('Homepage:');
	}
}
