<?php
namespace App\Presenters;
use Nette;
use Nette\Application\UI\Form;
use Test\Bs3FormRenderer;


class RefereePresenter extends BasePresenter
{
	private $database;
	public $Id; 

	public function __construct(Nette\Database\Context $databaza)
	{
		$this->database = $databaza;
	}

	public function actionDefault()
	{
		if(!$this->getUser()->isLoggedIn())
		{
			$this->redirect('Sign:in');
		}
	}

	public function actionDeleteReferee($jmeno)
	{
		$this->database->table('rozhodci')->get($jmeno)->delete();
		$this->presenter->flashMessage('Odstraněno', 'alert alert-success alert-dismissible');
		$this->redirect('Referee:');

	}
	public function renderDefault()
	{
		$this->template->rozhodci = $this->database->table('rozhodci');
	}

	protected function createComponentCreateForm()
	{
		$form = new Form;

		$renderer = $form->getRenderer();
		$renderer->wrappers['error']['container'] = 'div class="alert alert-danger"';
		$renderer->wrappers['controls']['container'] = null;
		$renderer->wrappers['pair']['container'] = 'div class="form-group"';
		$renderer->wrappers['pair']['.error'] = 'has-error';
		$renderer->wrappers['label']['container'] = 'div class="col-sm-2 control-label"';
		$renderer->wrappers['control']['description'] = 'span class=help-block';
		$renderer->wrappers['control']['errorcontainer'] = 'span class=help-block';
		$renderer->wrappers['control']['.text'] = 'form-control';
		$renderer->wrappers['control']['.password'] = 'form-control';
		$renderer->wrappers['control']['.file'] = 'form-control';
		$renderer->wrappers['control']['.email'] = 'form-control';
		$renderer->wrappers['control']['.number'] = 'form-control';
		$renderer->wrappers['control']['.button'] = 'btn btn-primary';
		$renderer->wrappers['control']['.submit'] = 'btn btn-primary';

		$form->addText('jmeno_R', '*Jméno:')->setRequired();
		$form->addText('narodnost', '*Národnost:')->setRequired()->addRule(Form::MAX_LENGTH, 'Označení země má maxilmálně 3 znaky', 3);
		$form->addSubmit('vytvorit', 'Vytvořit');

		$form->onSuccess[] = array($this, 'succ');
		return $form;
	}

	public function succ(Form $form, $values)
	{
		$this->database->table('rozhodci')->insert($values);
		$form->getPresenter()->flashMessage('Přidáno', 'alert alert-success alert-dismissible');
		$this->redirect('Referee:');
	}
}