<?php
namespace App\Forms;
use Nette;
use Nette\Application\UI\Form;
use Test\Bs3FormRenderer;

class EditTeamForm extends Nette\Object
{
	private $database;
	public $id;

	public function __construct(Nette\Database\Context $databaza)
	{
		$this->database = $databaza;
	}
	
	public function create()
	{
		$form = new Form;

		$renderer = $form->getRenderer();

 		$renderer->wrappers['error']['container'] = 'div class="alert alert-danger alert-dismissible"';
		$renderer->wrappers['controls']['container'] = null;
		$renderer->wrappers['pair']['container'] = 'div class="form-group"';
		$renderer->wrappers['pair']['.error'] = 'has-error';
		$renderer->wrappers['label']['container'] = 'div class="col-sm-2 control-label"';
		$renderer->wrappers['control']['description'] = 'span class=help-block';
		$renderer->wrappers['control']['errorcontainer'] = 'span class=help-block';
		$renderer->wrappers['control']['.text'] = 'form-control';
		$renderer->wrappers['control']['.password'] = 'form-control';
		$renderer->wrappers['control']['.file'] = 'form-control';
		$renderer->wrappers['control']['.email'] = 'form-control';
		$renderer->wrappers['control']['.number'] = 'form-control';
		$renderer->wrappers['control']['.button'] = 'btn btn-primary';
		$renderer->wrappers['control']['.submit'] = 'btn btn-primary';

		$skupiny = array();
		$skupiny['A'] = 'A';
		$skupiny['B'] = 'B';
		$skupiny['C'] = 'C';
		$skupiny['D'] = 'D';
		$form->addText('ID_tym', '*Skratka týmu')->addRule(Form::LENGTH,"Špatně", 3)->setRequired();
		$form->addText('trener', 'Hlavní trenér');
		$form->addText('asistenti', 'Asistent');
		$form->addSelect('skupina', 'Skupina', $skupiny)->setAttribute("class", "form-control");;
		$form->addText('skore', 'Skóre');
		$form->addText('pocet_bodu', 'Počet bodú');
		$form->addText('poradi', 'Pořadí');

		$form->addSubmit('edit', 'Změnit');

		$form->onSuccess[] = array($this, 'success');
		return $form;
	}

	public function success(Form $form, $values)
	{
		

		$this->database->table('tym')->get($this->id)->update($values);
		$form->getPresenter()->flashMessage('Úspěšne editováno!', 'alert alert-success alert-dismissible');

		$form->getPresenter()->redirect('Team:Show');
	}
}