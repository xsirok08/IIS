SET NAMES utf8;

SET foreign_key_checks = 0;

SET time_zone = 'SYSTEM';





DROP TABLE IF EXISTS zapas;

DROP TABLE IF EXISTS tym;

DROP TABLE IF EXISTS skupina;

DROP TABLE IF EXISTS vyrazovaci;

DROP TABLE IF EXISTS hrac;

DROP TABLE IF EXISTS udalost;

DROP TABLE IF EXISTS rozhodci;

DROP TABLE IF EXISTS faza;

DROP TABLE IF EXISTS je_pod_dozorom;

DROP TABLE IF EXISTS sa_zucastnil;

DROP TABLE IF EXISTS hral;

DROP TABLE IF EXISTS uzivatelia;



CREATE TABLE uzivatelia(

meno varchar(20) NOT NULL,

heslo varchar(20) NOT NULL,

rola varchar(10) NOT NULL,

PRIMARY KEY(meno)

)

ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;



CREATE TABLE zapas(

  domaci VARCHAR(10) NOT NULL,
  hostia VARCHAR(10) NOT NULL,
  ID_zapasu BIGINT NOT NULL AUTO_INCREMENT,

  misto_konani VARCHAR(20) NOT NULL,

  datum_konani VARCHAR(10) NOT NULL,

  vysledok VARCHAR(5),

  oznacenie VARCHAR(10),

  predlzenie VARCHAR(2),
  PRIMARY KEY(ID_zapasu)

)

ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;



CREATE TABLE tym(

  ID_tym VARCHAR(4) NOT NULL,

  trener VARCHAR(40) NOT NULL,

  asistenti VARCHAR(80) NOT NULL,

  pocet_bodu BIGINT NOT NULL,

  skore VARCHAR(10) NOT NULL,

  poradi BIGINT NOT NULL,

  skupina VARCHAR(1),

  PRIMARY KEY(ID_tym)

)

ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;



CREATE TABLE faza(

oznacenie VARCHAR(10),

PRIMARY KEY(oznacenie)

)

ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;



CREATE TABLE hrac(

  rodne_cislo BIGINT NOT NULL,

  jmeno_H VARCHAR(40) NOT NULL,

  klub VARCHAR(40) NOT NULL,

  pozice VARCHAR(2),

  cislo_dresu BIGINT NOT NULL,

  vek BIGINT NOT NULL,

  ID_tym VARCHAR(4) NOT NULL,

  PRIMARY KEY(rodne_cislo)

)

ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;



CREATE TABLE udalost(

  ID_udalosti BIGINT NOT NULL AUTO_INCREMENT,

  cas BIGINT NOT NULL,

  druh VARCHAR(40) NOT NULL,

  rodne_cislo BIGINT NOT NULL,

  jmeno_R VARCHAR(40) NOT NULL,

  ID_zapasu BIGINT NOT NULL,

  PRIMARY KEY(ID_udalosti)

)

ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;



CREATE TABLE rozhodci(

  jmeno_R VARCHAR(40) NOT NULL,

  narodnost VARCHAR(3) NOT NULL,

  PRIMARY KEY(jmeno_R)

)

ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;



CREATE TABLE je_pod_dozorom(

  jmeno_R VARCHAR(40) NOT NULL,

  ID_zapasu BIGINT NOT NULL,

  pozicia VARCHAR(3) NOT NULL,

  PRIMARY KEY(jmeno_R, ID_zapasu)

  )

ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;




 ALTER TABLE zapas ADD FOREIGN KEY (oznacenie) references faza(oznacenie) ON UPDATE CASCADE ON DELETE CASCADE;



 ALTER TABLE hrac ADD FOREIGN KEY (ID_tym) references tym(ID_tym) ON UPDATE CASCADE ON DELETE CASCADE;



 ALTER TABLE udalost ADD FOREIGN KEY (ID_zapasu) references zapas(ID_zapasu) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE udalost ADD FOREIGN KEY (jmeno_R) references rozhodci(jmeno_R) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE udalost ADD FOREIGN KEY (rodne_cislo) references hrac(rodne_cislo) ON UPDATE CASCADE ON DELETE CASCADE;




 ALTER TABLE je_pod_dozorom ADD FOREIGN KEY(jmeno_R) references rozhodci(jmeno_R) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE je_pod_dozorom ADD FOREIGN KEY(ID_zapasu) references zapas(ID_zapasu) ON UPDATE CASCADE ON DELETE CASCADE;




 INSERT INTO uzivatelia(meno, heslo, rola) VALUES('admin', 'admin', 'admin');
 INSERT INTO uzivatelia(meno, heslo, rola) VALUES('uzivatel', 'uzivatel', 'uzivatel');


  INSERT INTO faza(oznacenie) VALUES('SKUPINA');
  INSERT INTO faza(oznacenie) VALUES('STVRTFINALE');
  INSERT INTO faza(oznacenie) VALUES('SEMIFINALE');
  INSERT INTO faza(oznacenie) VALUES('FINALE');
  INSERT INTO faza(oznacenie) VALUES('O TRETI');



 INSERT INTO zapas(misto_konani, datum_konani, vysledok, oznacenie, predlzenie, domaci, hostia) VALUES('PRAHA', '02.05.2017', '4:2', 'SKUPINA', 'pr', 'CZE', 'SVK');
 INSERT INTO zapas(misto_konani, datum_konani, vysledok, oznacenie, predlzenie, domaci, hostia) VALUES('BRNO', '03.05.2017', '2:1', 'SKUPINA', '-', 'FIN', 'RUS');
INSERT INTO zapas(misto_konani, datum_konani, vysledok, oznacenie, predlzenie, domaci, hostia) VALUES('PRAHA', '04.05.2017', '4:2', 'SKUPINA', 'sn', 'USA', 'CAN');
 INSERT INTO zapas(misto_konani, datum_konani, vysledok, oznacenie, predlzenie, domaci, hostia) VALUES('BRNO', '05.05.2017', '7:1', 'SKUPINA', '-', 'SVK', 'RUS');
INSERT INTO zapas(misto_konani, datum_konani, vysledok, oznacenie, predlzenie, domaci, hostia) VALUES('PRAHA', '06.05.2017', '1:3', 'SKUPINA', 'pr', 'FIN', 'CZE');
 INSERT INTO zapas(misto_konani, datum_konani, vysledok, oznacenie, predlzenie, domaci, hostia) VALUES('BRNO', '08.05.2017', '2:5', 'SKUPINA', '-', 'SWE', 'CAN');


 INSERT INTO tym(ID_tym, trener, asistenti, pocet_bodu, skore, poradi, skupina) VALUES('CZE', 'Vujtek', 'Zeleny', '3', '8:1:1', '2', 'A');
INSERT INTO tym(ID_tym, trener, asistenti, pocet_bodu, skore, poradi, skupina) VALUES('USA', 'Babcock', 'ONeil', '6', '11:5:2', '1', 'C');
INSERT INTO tym(ID_tym, trener, asistenti, pocet_bodu, skore, poradi, skupina) VALUES('CAN', 'Morris', 'Smith', '3', '4:7:0', '3', 'D');
INSERT INTO tym(ID_tym, trener, asistenti, pocet_bodu, skore, poradi, skupina) VALUES('SWE', 'Alf', 'Daffson', '4', '4:2:0', '1', 'C');
 INSERT INTO tym(ID_tym, trener, asistenti, pocet_bodu, skore, poradi, skupina) VALUES('SVK', 'Ciger', 'Modry', '0', '2:4:0', '4', 'A');

 INSERT INTO tym(ID_tym, trener, asistenti, pocet_bodu, skore, poradi, skupina) VALUES('RUS', 'Rykov', 'Vasilievski', '2', '1:1:1', '2', 'B');

 INSERT INTO tym(ID_tym, trener, asistenti, pocet_bodu, skore, poradi, skupina) VALUES('FIN', 'Kalainen', 'Puskonen', '1', '1:1:0', '3', 'B');



 INSERT INTO hrac(rodne_cislo, jmeno_H, klub, pozice, cislo_dresu, vek, ID_tym) VALUES('9512037260', 'Hossa', 'Blackhawks', 'LK', '81', '38', 'SVK');

 INSERT INTO hrac(rodne_cislo, jmeno_H, klub, pozice, cislo_dresu, vek, ID_tym) VALUES('9512037271', 'Hertl', 'Sharks', 'CR', '22', '26', 'CZE');

 INSERT INTO hrac(rodne_cislo, jmeno_H, klub, pozice, cislo_dresu, vek, ID_tym) VALUES('9512037282', 'Tatar', 'Red Wings', 'CR', '21', '26', 'SVK');

 INSERT INTO hrac(rodne_cislo, jmeno_H, klub, pozice, cislo_dresu, vek, ID_tym) VALUES('9512037293', 'Malkin', 'Penguins', 'PK', '42', '33', 'RUS');

 INSERT INTO hrac(rodne_cislo, jmeno_H, klub, pozice, cislo_dresu, vek, ID_tym) VALUES('9512037304', 'Barkov', 'Panthers', 'LK', '31', '24', 'FIN');

 INSERT INTO hrac(rodne_cislo, jmeno_H, klub, pozice, cislo_dresu, vek, ID_tym) VALUES('9512037315', 'Laine', 'Jets', 'PK', '36', '19', 'FIN');

 INSERT INTO hrac(rodne_cislo, jmeno_H, klub, pozice, cislo_dresu, vek, ID_tym) VALUES('9512345455', 'Karlsson', 'Senators', 'D', '16', '29', 'SWE');
 INSERT INTO hrac(rodne_cislo, jmeno_H, klub, pozice, cislo_dresu, vek, ID_tym) VALUES('9512037356', 'Burns', 'Sharks', 'D', '32', '32', 'CAN');
 INSERT INTO hrac(rodne_cislo, jmeno_H, klub, pozice, cislo_dresu, vek, ID_tym) VALUES('9512037376', 'Price', 'Canadiens', 'GK', '30', '19', 'CAN');
 INSERT INTO hrac(rodne_cislo, jmeno_H, klub, pozice, cislo_dresu, vek, ID_tym) VALUES('9234534534', 'Benn', 'Stars', 'CR', '42', '25', 'USA');
 INSERT INTO hrac(rodne_cislo, jmeno_H, klub, pozice, cislo_dresu, vek, ID_tym) VALUES('9512037878', 'D.Sedin', 'Canucks', 'PK', '39', '19', 'SWE');
 INSERT INTO hrac(rodne_cislo, jmeno_H, klub, pozice, cislo_dresu, vek, ID_tym) VALUES('9512037234', 'Voráček', 'Flyers', 'PK', '33', '19', 'CZE');
 INSERT INTO hrac(rodne_cislo, jmeno_H, klub, pozice, cislo_dresu, vek, ID_tym) VALUES('9512037345', 'Jágr', 'Flames', 'CR', '74', '50', 'CZE');
 INSERT INTO hrac(rodne_cislo, jmeno_H, klub, pozice, cislo_dresu, vek, ID_tym) VALUES('9512037111', 'Halák', 'Islanders', 'GK', '24', '34', 'SVK');
 INSERT INTO hrac(rodne_cislo, jmeno_H, klub, pozice, cislo_dresu, vek, ID_tym) VALUES('9512037222', 'Pavelski', 'Sharks', 'LK', '56', '33', 'USA');
 INSERT INTO hrac(rodne_cislo, jmeno_H, klub, pozice, cislo_dresu, vek, ID_tym) VALUES('9512234543', 'Murray', 'Penguins', 'GK', '32', '22', 'CAN');
 INSERT INTO hrac(rodne_cislo, jmeno_H, klub, pozice, cislo_dresu, vek, ID_tym) VALUES('9512037555', 'Ovechkin', 'Capitals', 'PK', '36', '35', 'RUS');
 INSERT INTO hrac(rodne_cislo, jmeno_H, klub, pozice, cislo_dresu, vek, ID_tym) VALUES('9512037666', 'Kucherov', 'Lightnings', 'PK', '36', '26', 'RUS');






  INSERT INTO rozhodci(jmeno_R, narodnost) VALUES('Varga', 'SK');

  INSERT INTO rozhodci(jmeno_R, narodnost) VALUES('Connor', 'USA');

  INSERT INTO rozhodci(jmeno_R, narodnost) VALUES('Duke', 'CAN');

  INSERT INTO rozhodci(jmeno_R, narodnost) VALUES('Falalainen', 'SWE');

  INSERT INTO rozhodci(jmeno_R, narodnost) VALUES( 'Waffleson', 'SWE');

  INSERT INTO rozhodci(jmeno_R, narodnost) VALUES('Lievanson', 'FIN');

  INSERT INTO rozhodci(jmeno_R, narodnost) VALUES('Zeman', 'CZE');

  INSERT INTO rozhodci(jmeno_R, narodnost) VALUES('Ovcacek', 'CZE');



  INSERT INTO udalost( cas, druh, rodne_cislo, jmeno_R, ID_zapasu) VALUES('17', 'sekanie', '9512037260', 'Ovcacek', '1');

  INSERT INTO udalost(cas, druh, rodne_cislo, jmeno_R, ID_zapasu) VALUES('36', 'hakovanie', '9512037282', 'Lievanson', '1');

  INSERT INTO udalost( cas, druh, rodne_cislo, jmeno_R, ID_zapasu) VALUES('5', 'zakazane uvolnenie', '9512037293', 'Falalainen', '2');

  INSERT INTO udalost(cas, druh, rodne_cislo, jmeno_R, ID_zapasu) VALUES('8', 'offsajd', '9512037304', 'Duke', '2');

  INSERT INTO udalost(cas, druh, rodne_cislo, jmeno_R, ID_zapasu) VALUES('55', 'offsajd', '9512037315', 'Duke', '2');

  INSERT INTO udalost(cas, druh, rodne_cislo, jmeno_R, ID_zapasu) VALUES('7', 'offsajd', '9512037271', 'Waffleson', '1');

  INSERT INTO udalost(cas, druh, rodne_cislo, jmeno_R, ID_zapasu) VALUES('43', 'gol', '9512037222', 'Connor', '3');
  INSERT INTO udalost(cas, druh, rodne_cislo, jmeno_R, ID_zapasu) VALUES('43', 'gol', '9512037666', 'Ovcacek', '4');
  INSERT INTO udalost(cas, druh, rodne_cislo, jmeno_R, ID_zapasu) VALUES('22', 'gol', '9512037315', 'Varga', '6');
  INSERT INTO udalost(cas, druh, rodne_cislo, jmeno_R, ID_zapasu) VALUES('56', 'gol', '9512037555', 'Duke', '4');
  INSERT INTO udalost(cas, druh, rodne_cislo, jmeno_R, ID_zapasu) VALUES('22', 'gol', '9512345455', 'Falalainen', '5');
  INSERT INTO udalost(cas, druh, rodne_cislo, jmeno_R, ID_zapasu) VALUES('11', 'gol', '9512037555', 'Falalainen', '4');
  INSERT INTO udalost(cas, druh, rodne_cislo, jmeno_R, ID_zapasu) VALUES('44', 'gol', '9512037315', 'Varga', '3');
  INSERT INTO udalost(cas, druh, rodne_cislo, jmeno_R, ID_zapasu) VALUES('52', 'gol', '9512345455', 'Waffleson', '6');
  INSERT INTO udalost(cas, druh, rodne_cislo, jmeno_R, ID_zapasu) VALUES('1', 'gol', '9512037345', 'Zeman', '5');
  INSERT INTO udalost(cas, druh, rodne_cislo, jmeno_R, ID_zapasu) VALUES('33', 'gol', '9512037345', 'Zeman', '5');
  INSERT INTO udalost(cas, druh, rodne_cislo, jmeno_R, ID_zapasu) VALUES('4', 'gol', '9512037356', 'Duke', '3');



  INSERT INTO je_pod_dozorom(jmeno_R, ID_zapasu, pozicia) VALUES('Ovcacek', '1', 'V');

  INSERT INTO je_pod_dozorom(jmeno_R, ID_zapasu, pozicia) VALUES('Zeman', '1', 'H');

  INSERT INTO je_pod_dozorom(jmeno_R, ID_zapasu, pozicia) VALUES('Lievanson', '1', 'V');

  INSERT INTO je_pod_dozorom(jmeno_R, ID_zapasu, pozicia) VALUES('Waffleson', '1', 'V');

  INSERT INTO je_pod_dozorom(jmeno_R, ID_zapasu, pozicia) VALUES('Falalainen', '2', 'V');

  INSERT INTO je_pod_dozorom(jmeno_R, ID_zapasu, pozicia) VALUES('Duke', '2', 'H');

  INSERT INTO je_pod_dozorom(jmeno_R, ID_zapasu, pozicia) VALUES('Connor', '2', 'V');

  INSERT INTO je_pod_dozorom(jmeno_R, ID_zapasu, pozicia) VALUES('Varga', '2', 'V');





