function sortTable(id, n) {
	var table, rows, switched, i, x, y, doswitch = 0;
	if (typeof sortTable.switchcount == 'undefined') {
		sortTable.switchcount = new Array();
	}

	table = document.getElementById(id);
	switched = true;

	if (typeof sortTable.switchcount[n] == 'undefined') {
		sortTable.switchcount[n] = 0;
	} else {
		sortTable.switchcount[n]++;
	}

	while (switched) {
    		switched = false;
		rows = table.getElementsByTagName("TR");
		for (i = 1; i < (rows.length - 1); i++) {
      			doswitch = false;

      			x = rows[i].getElementsByTagName("TD")[n];
     			y = rows[i + 1].getElementsByTagName("TD")[n];

			if (sortTable.switchcount[n] % 2) {
				if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
					doswitch = true;
					break;
				}
			} else {
				if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
					doswitch= true;
					break;
				}
			}
		}

		if (doswitch) {
			rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
			switched = true;
		}
	}
}
